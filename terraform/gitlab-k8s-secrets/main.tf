locals {
  secret_name = "gitlab-registry"
  dockercfg = {
    auths = {
      "registry.gitlab.com" = {
        "username" = var.gitlab_deploy_user
        "password" = var.gitlab_deploy_password
        "email"    = "pxo.automation@panaxeo.com"
        "auth"     = base64encode("${var.gitlab_deploy_user}:${var.gitlab_deploy_password}")
      }
    }
  }
}

resource "kubernetes_secret" "gitlab_registry" {
  type = "kubernetes.io/dockerconfigjson"

  metadata {
    name      = local.secret_name
    namespace = var.namespace
  }

  data = {
    ".dockerconfigjson" = jsonencode(local.dockercfg)
  }
}


