terraform {
  required_version = ">= 0.13"
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "1.13.3"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.2.0"
    }
  }
}

provider "kubernetes" {
  # Configuration options
}
