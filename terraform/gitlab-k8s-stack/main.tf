resource "kubernetes_service" "main" {
  metadata {
    name      = var.name
    namespace = var.namespace
  }

  spec {
    selector = {
      name = var.name
    }

    port {
      port        = 80
      target_port = var.container_port
    }
  }
}

resource "kubernetes_deployment" "main" {
  metadata {
    name      = var.name
    namespace = var.namespace
  }

  spec {
    selector {
      match_labels = {
        name = var.name
      }
    }

    template {
      metadata {
        labels = {
          name = var.name
        }
      }

      spec {
        container {
          name  = var.container_name
          image = var.container_image

          port {
            container_port = var.container_port
          }

          liveness_probe {
            http_get {
              path = "/healthcheck"
              port = var.container_port
            }

            initial_delay_seconds = "10"
            period_seconds        = "10"
          }

          readiness_probe {
            http_get {
              path = "/healthcheck"
              port = var.container_port
            }

            initial_delay_seconds = "10"
            period_seconds        = "10"
          }

          resources {
            requests {
              cpu    = "0.01"
              memory = "16Mi"
            }

            limits {
              cpu    = "0.1"
              memory = "64Mi"
            }
          }
        }

        image_pull_secrets {
          name = "gitlab-registry"
        }
      }
    }
  }
}
