variable "name" {}
variable "container_name" {
  default = "main"
}
variable "container_image" {}
variable "container_port" {
  default = 80
}
variable "namespace" {}
variable "ingress_domain" {
  type    = string
  default = null
}
